//
//  PersonTableExtension.swift
//  AtomRPGWiki
//
//  Created by Danya on 10.12.2021.
//

import UIKit

extension UITableView {
    func registerCustomCell(_ cell: CustomCell.Type) {
        
        register(CustomTableViewCell.cellNib(), forCellReuseIdentifier: CustomTableViewCell.cellIndentifire()!)
        
    }
}

extension PersonViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return person.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.cellIndentifire()!,
                                                 for: indexPath) as! CustomTableViewCell
        
        let model = person[indexPath.row]
        cell.imegeView.layer.cornerRadius = cell.imegeView.frame.size.height / 2
        cell.configure(with: model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
