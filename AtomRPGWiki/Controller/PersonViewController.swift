//
//  PersonViewController.swift
//  AtomRPGWiki
//
//  Created by Danya on 10.12.2021.
//

import UIKit

class PersonViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var person: [AtomModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.registerCustomCell(CustomTableViewCell.self)
        
        person.append(AtomModel(image: "Alexander", title: "Александр", description: "Кадет Атома"))
        person.append(AtomModel(image: "Alexander", title: "Александр", description: "Кадет Атома"))
        person.append(AtomModel(image: "Alexander", title: "Александр", description: "Кадет Атома"))
        person.append(AtomModel(image: "Alexander", title: "Александр", description: "Кадет Атома"))
        person.append(AtomModel(image: "Alexander", title: "Александр", description: "Кадет Атома"))
        
    }
}
