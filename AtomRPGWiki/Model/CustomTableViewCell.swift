//
//  CustomTableViewCell.swift
//  AtomRPGWiki
//
//  Created by Danya on 10.12.2021.
// кастомная ячейка, которую буду использовать как образец для всей таблицы

import UIKit

protocol CustomCell {
    static func cellNib() -> UINib?
    static func cellIndentifire() -> String?
} //регистрация XIB и Identifire

class CustomTableViewCell: UITableViewCell, CustomCell {
    
    @IBOutlet weak var imegeView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure(with object: AtomModel) {
        
        imegeView.image = UIImage(named: object.image)
        titleLabel.text = object.title
        descriptionLabel.text = object.description
    }
    
    static func cellNib() -> UINib? {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static func cellIndentifire() -> String? {
        return String(describing: self)
    }
    
}

