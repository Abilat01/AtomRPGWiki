//
//  AtomModel.swift
//  AtomRPGWiki
//
//  Created by Danya on 10.12.2021.
//

import UIKit


struct AtomModel {
    
    var image: String
    var title: String
    var description: String
    
}
